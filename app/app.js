Ext.application({
    name: 'Evento',
    models: ['Alert', 'Archive', 'Upcoming'],
    stores: ['Alerts', 'Archives', 'Upcoming'],
    controllers: ['Main'],
    viewport: {
        layout: 'card',
    },
    launch: function () {
        Ext.Viewport.add([
            {
                xtype: 'evento_splash'
            },
            {
                xtype: 'app_viewport'
            }]);

        Ext.defer(function () {
            Ext.Viewport.setActiveItem(1)

        }, 2000);
    }
});