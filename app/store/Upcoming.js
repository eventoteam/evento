Ext.define('Evento.store.Upcoming',{
    extend: 'Ext.data.Store',
    config:{
        model: 'Evento.model.Upcoming',
        proxy: {
            type: 'ajax',
            url : 'app/data/upcoming.json',
            reader:{
                type:'json',   
                rootProperty:'upcoming'
            }
        },
        autoLoad: true,
    }
});
