Ext.define('Evento.store.Archives', {
    extend: 'Ext.data.Store',
    config: {
        model: 'Evento.model.Archive',
        proxy: {
            type: 'ajax',
            url: 'app/data/archives.json',
            reader: {
                type: 'json',
                rootProperty: 'archives'
            }
        },
        autoLoad: true
    }
});