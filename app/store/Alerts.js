Ext.define('Evento.store.Alerts', {
    extend: 'Ext.data.Store',
    config: {
        model: 'Evento.model.Alert',
        proxy: {
            type: 'ajax',
            url: 'app/data/alerts.json',
            reader: {
                type: 'json',
                rootProperty: 'alerts'
            }
        },
        autoLoad: true
    }
});