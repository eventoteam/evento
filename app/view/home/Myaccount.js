Ext.define('Evento.view.home.Myaccount', {
    extend: 'Ext.form.Panel',
    xtype: 'myaccount',
    config: {
        items: [{
            xtype: 'fieldset',
            title: 'MyAccount',
            defaults: {
                xtype: 'textfield',
                labelWidth: '30%'
            },
            items: [
                {
                    name: 'E-mail',
                    label: 'E-mail',
                    placeHolder: 'e-mail'
                }, {
                    name: 'Mobile number',
                    label: 'Mobile number',
                    placeHolder: 'mobile number'
                }
            ]
        }]
    }
});