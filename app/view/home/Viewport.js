Ext.define('Evento.view.home.Viewport', {
    extend: 'Ext.Container',
    xtype: 'app_viewport',
    config: {
        fullscreen: true,
        layout: 'hbox',
        items: [{
                xtype: 'evento_container',
                cls: 'slide',
                width: '100%'
                }, {
                xtype: 'navigation',
                width: 180
                }
               ]
    }
});