Ext.define('Evento.view.home.Container', {
    extend: 'Ext.navigation.View',
    xtype: 'evento_container',
    config: {
        defaultBackButtonText: '',
        navigationBar: {
            backButton: {
                iconCls: 'nav gallery-left',
                ui: 'plain',
                id: 'backButtonId'
            },
            items: [{
                align: 'left',
                name: 'nav_btn',
                iconCls: 'more',
                iconMask: true,
                ui: 'plain'
                }, {
                xtype: 'panel',
                html: '<img style="height: 20px;" src="res/images/Icon.png"/>',
                align: 'right'
                }],
        },
        items: {
            title: 'Evento',
            items: [{
                xtype: 'upcommingList',
                height: '70%'
                    }, {
                xtype: 'evento_advhome'
                    }]
        }
    }
});