Ext.define('Evento.view.home.Splash', {
    extend: 'Ext.Panel',
    xtype: 'evento_splash',
    config: {
        style: 'background-color:white;font-size:30pt;color:#C1E1A6;font-weight:bold;',
        layout: {
            type: 'vbox',
            align: 'middle',
            pack: 'center'
        },
        items: [{
                html: '<span style="color: #888;" class="icon-bullhorn"></span>'
            },
            {
                html: 'Evento',
                style: "font-family:'Exo 2', sans-serif"
            }
              ]
    }
});