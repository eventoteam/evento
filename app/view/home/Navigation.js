Ext.define('Evento.view.home.Navigation', {
    extend: 'Ext.Container',
    xtype: 'navigation',
    config: {
        cls: 'mainmenu',
        docked: 'left',
        top: 0,
        left: 0,
        bottom: 0,
        zIndex: 0,
        width: 266,
        padding: '97 0 0 0',
        scrollable: 'vertical',
        defaultType: 'button',
        defaults: {
            textAlign: 'left'
        },
        items: [{
            text: 'Home',
            ui: 'mainmenu',
            itemId: 'home',
            iconCls: 'ico-home'
        }, {
            xtype: 'component',
            cls: 'divider',
            html: 'Events'
        }, {
            text: 'Upcoming',
            ui: 'mainmenu',
            itemId: 'upcoming',
            iconCls: 'ico-upcoming'
        }, {
            text: 'Archive',
            ui: 'mainmenu',
            itemId: 'archive',
            iconCls: 'ico-archive'
        }, {
            text: 'Alert',
            ui: 'mainmenu',
            itemId: 'alert',
            iconCls: 'ico-alert'
        }, {
            xtype: 'component',
            cls: 'divider'
        }, {
            text: 'MyAccount',
            ui: 'mainmenu',
            itemId: 'myaccount',
            iconCls: 'ico-myaccount'
        }, {
            text: 'Logout',
            ui: 'mainmenu',
            itemId: 'logout',
            iconCls: 'ico-logout'
        }]
    }
});