Ext.define('Evento.view.event.EventDetails', {
    extend: 'Ext.Panel',
    xtype: 'eventDetail',
    config: {
        layout: {
            type: 'vbox',
        },
        scrollable: {
            direction: 'vertical'
        },
        items: [{
                flex: 8,
                xtype: 'event'
               }, {
                xtype: 'spacer',
                flex: 1
               }, {
                flex: 10,
                xtype: 'confirm'
               }, {
                xtype: 'spacer',
                flex: 1
               }, {
                flex: 21,
                xtype: 'upcomAgenda'
               }, {
                xtype: 'spacer',
                flex: 1
               }, {
                flex: 22,
                xtype: 'upcomAttach'
               }
        ]
    }
});