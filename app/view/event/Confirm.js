Ext.define('Evento.view.event.Confirm', {
    extend: 'Ext.Panel',
    xtype: 'confirm',
    config: {
        style: 'border-top: 1px solid black; border-bottom: 1px solid black;',
        layout: 'hbox',
        items: [
            {
                xtype: 'spacer'
        }, {
                xtype: 'button',
                margin: 20,
                text: '<font size=2>Accept</font>',
                action: 'AcceptBtn',
                flex: 1
        }, {
                xtype: 'spacer'
        }, {
                xtype: 'button',
                margin: 20,
                text: '<font size=2>Reject</font>',
                action: 'RejectBtn',
                flex: 1
        }, {
                xtype: 'spacer'
        }]
    }
});