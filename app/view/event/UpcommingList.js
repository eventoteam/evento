Ext.define('Evento.view.event.UpcommingList', {
    extend: 'Ext.Panel',
    xtype: 'upcommingList',
    config: {
        layout: 'card',
        items: {
            xtype: 'list',
            id: 'upcominglist',
            store: 'Upcoming',
            itemTpl: Ext.create('Ext.XTemplate',
                '<div>',
                '<div class=list_div>{name}</div>',
                '<div class=list_div>{date}</div>',
                '</div>',
                '<div>',
                '<div class=list_div>{venue}</div>',
                '</div>',
                '<div style="clear:both"></div>'
            )
        }
    }
});