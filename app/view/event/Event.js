Ext.define('Evento.view.event.Event', {
    extend: 'Ext.Panel',
    xtype: 'event',
    config: {
        style: 'border-bottom: 1px solid black;',
        styleHtmlContent: true,
        tpl: new Ext.XTemplate(
            '<div>',
            '<div style="float:left; width: 20%">{name}</div>',
            '<div style="float:left;width: 50%">{date}</div>',
            '</div>',
            '<div>',
            '<div style="float:left; width: 100%">{venue}</div>',
            '</div>',
            '<div style="clear:both"></div>'
        )
    }
});