Ext.define('Evento.view.event.ArchiveAgenda', {
    extend: 'Ext.Panel',
    xtype: 'archiveAgenda',
    config: {
        cls: 'panel_layout',
        scrollable: true,
        tpl: new Ext.XTemplate(
            '<table class=table>',
            '<tpl for="agenda">',
            '<tr>',
            '<td width="20%"></td>',
            '<td width="30%"> {item} </td>',
            '<td width="15%"></td>',
            '<td width="35%">{desc}</td>',
            '</tr>',
            '</tpl>',
            '</table>'
        ),
        items: [{
            xtype: 'titlebar',
            docked: 'top',
            title: '<div>Agenda</div>',
            style: {
                'font-size': '14px'
            }
        }]
    }
});