Ext.define('Evento.view.event.AlertList', {
    extend: 'Ext.Panel',
    xtype: 'alertList',
    config: {
        layout: 'card',
        items: {
            xtype: 'list',
            itemId: 'alertsList',
            store: 'Alerts',
            itemTpl: Ext.create('Ext.XTemplate',
                '<div>',
                '<div class=list_div>{name}</div>',
                '<div class=list_div>{date}</div>',
                '</div>',
                '<div style="clear:both"></div>'
            )
        }
    }
});