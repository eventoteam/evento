Ext.define('Evento.view.event.ArchiveDetails', {
    extend: 'Ext.Panel',
    xtype: 'archiveDetails',
    config: {
        layout: {
            type: 'vbox',
        },
        scrollable: {
            direction: 'vertical',
        },
        items: [{
                flex: 8,
                xtype: 'event'
               }, {
                xtype: 'spacer',
                flex: 1
               }, {
                flex: 16,
                xtype: 'archiveNote'
               }, {
                xtype: 'spacer',
                flex: 1
               }, {
                flex: 16,
                xtype: 'archiveAgenda'
               }, {
                xtype: 'spacer',
                flex: 1
               }, {
                flex: 16,
                xtype: 'archiveAttach'
               }
        ]
    }
});