Ext.define('Evento.view.event.ArchiveList', {
    extend: 'Ext.Panel',
    xtype: 'archiveList',
    config: {
        layout: 'card',
        items: {
            xtype: 'list',
            id: 'archivelist',
            store: 'Archives',
            itemTpl: Ext.create('Ext.XTemplate',
                '<div>',
                '<div class=list_div>{name}</div>',
                '<div class=list_div>{date}</div>',
                '</div>',
                '<div>',
                '<div class=list_div>{venue}</div>',
                '</div>',
                '<div style="clear:both"></div>'
            )
        }
    }
});