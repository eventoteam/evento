Ext.define('Evento.view.event.ArchiveAttach', {
    extend: 'Ext.Panel',
    xtype: 'archiveAttach',
    config: {
        cls: 'panel_layout',
        scrollable: true,
        tpl: new Ext.XTemplate(
            '<table class=table>',
            '<tpl for="attachment">',
            '<tr>',
            '<td width="20%" height="30"></td>',
            '<td width="30%">{name} </td>',
            '<td width="15%" height="30"></td>',
            '<td width="35%">{url}</td>',
            '</tr>',
            '</tpl>',
            '</table>'
        ),
        items: [{
            xtype: 'titlebar',
            docked: 'top',
            title: '<div>Attachment</div>',
            style: {
                'font-size': '14px'
            }
        }]
    }
});