Ext.define('Evento.view.event.ArchiveNote', {
    extend: 'Ext.Panel',
    xtype: 'archiveNote',
    config: {
        cls: 'panel_layout',
        scrollable: true,
        tpl: new Ext.XTemplate(
            '<table class=table>',
            '<tpl for="note">',
            '<tr>',
            '<td width="20%"></td>',
            '<td width="30%">{slno} </td>',
            '<td width="15%"></td>',
            '<td width="35%">{note}</td>',
            '</tr>',
            '</tpl>',
            '</table>'
        ),
        items: [{
            xtype: 'titlebar',
            docked: 'top',
            title: '<div>Note</div>',
            style: {
                'font-size': '14px'
            }
        }]
    }
});