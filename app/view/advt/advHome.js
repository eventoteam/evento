Ext.define('Evento.view.advt.AdvHome', {
    extend: 'Ext.Panel',
    xtype: 'evento_advhome',
    config: {
        style: 'background-color: #F8F8F8',
        height: '30%',
        layout: 'fit',
        cls: 'carousel-image'
    },
    initialize: function () {
         this.callParent(arguments);
        var panel = this;
        var store = new Ext.create('Ext.data.Store', {
            fields: ['name', 'url'],
            proxy: {
                type: 'ajax',
                url: 'app/data/advts.json',
                reader: {
                    type: 'json',
                    rootProperty: 'advts'
                }
            },
            listeners: {
                refresh: function (data) {
                    var items = [];
                    data.each(function (record) {
                        items.push({
                            html: '<img src="' + record.get('url') + '"/>'
                        });
                    });
                    var carousel = new Evento.view.advt.RotatingCarousel({
                        items: items
                    });
                    panel.add(carousel);
                }
            }
        });
        store.load();
    }
});