var eventname = null;
Ext.define('Evento.controller.Main', {
    extend: 'Ext.app.Controller',
    config: {
        views: ['home.Splash',
                'home.Container',
                'home.Navigation',
                'home.Viewport',
                'home.Myaccount',
                'advt.AdvHome',
                'advt.RotatingCarousel',
                'event.UpcommingList',
                'event.ArchiveList',
                'event.AlertList',
                'event.EventDetails',
                'event.Event',
                'event.Confirm',
                'event.ArchiveDetails',
                'event.ArchiveNote',
                'event.UpcomAgenda',
                'event.UpcomAttach',
                'event.ArchiveAgenda',
                'event.ArchiveAttach',
                'event.Alert'
        ],
        refs: {
            eventoContainer: 'evento_container',
            alertsList: 'list[itemId = alertsList]',
            upcomingList: 'list[itemId = upcominglist]',
            archiveList: 'list[itemId = archivelist]',
            btnAccept: 'button[action = AcceptBtn]',
            btnReject: 'button[action = RejectBtn]',
            navBtn: 'button[name = nav_btn]',
            upcoming: 'button[itemId = upcoming]',
            archive: 'button[itemId = archive]',
            alert: 'button[itemId = alert]',
            myaccount: 'button[itemId = myaccount]',
            logout: 'button[itemId = logout]',
            eventoHome: 'button[itemId = home]'
        },
        control: {
            upcomingList: {
                itemtap: 'onUpcomingItemTap'
            },
            archiveList: {
                itemtap: 'onArchiveItemTap'
            },
            alertsList: {
                itemtap: 'onAlertItemTap'
            },
            btnAccept: {
                tap: 'onAcceptTap'
            },
            btnReject: {
                tap: 'onRejectTap'
            },
            navBtn: {
                tap: 'toggleNav'
            },
            upcoming: {
                tap: 'upcomingTap'
            },
            archive: {
                tap: 'archiveTap'
            },
            alert: {
                tap: 'alertTap'
            },
            myaccount: {
                tap: 'myaccountTap'
            },
            logout: {
                tap: 'logoutTap'
            },
            eventoHome: {
                tap: 'eventoTap'
            }
        }
    },

    onUpcomingItemTap: function (list, index, target, record) {
        eventname = record.get('name');
        this.getEventoContainer().push({
            xtype: 'eventDetail',
            title: 'Evento'
        });
        var container = this.getEventoContainer();
        container.down('.event').setData(record.data);
        container.down('.upcomAgenda').setData(record.data);
        container.down('.upcomAttach').setData(record.data);
        this.hideBackBtn();
    },

    onArchiveItemTap: function (list, index, target, record) {
        this.getEventoContainer().push({
            xtype: 'archiveDetails',
            title: 'Evento'
        });
        var container = this.getEventoContainer();
        container.down('.event').setData(record.data);
        container.down('.archiveNote').setData(record.data);
        container.down('.archiveAgenda').setData(record.data);
        container.down('.archiveAttach').setData(record.data);
        this.hideBackBtn();
    },

    onAlertItemTap: function (list, index, target, record) {
        this.getEventoContainer().push({
            xtype: 'alert',
            title: 'Evento',
            data: record.data
        });
        this.hideBackBtn();
    },

    hideBackBtn: function () {
        this.getEventoContainer().getNavigationBar().getBackButton().on('tap', function (self) {
            Ext.getCmp('backButtonId').hide();
        });
    },

    onAcceptTap: function () {
        Ext.Msg.alert("Accept " + eventname);
    },

    onRejectTap: function () {
        Ext.Msg.alert("Reject " + eventname);
    },
    
    eventoTap: function () {
        var container = this.getEventoContainer();
        container.removeAll();
        container.push({
            title: 'Evento',
            items: [{
                xtype: 'upcommingList',
                height: '70%'
                }, {
                xtype: 'evento_advhome'
            }]
        });
        Ext.getCmp('backButtonId').hide();
        this.toggleNav();
    },

    upcomingTap: function () {
        var container = this.getEventoContainer();
        container.removeAll();
        container.push({
            title: 'Evento',
            xtype: 'upcommingList'
        });
        Ext.getCmp('backButtonId').hide();
        this.toggleNav();
    },
    archiveTap: function () {
        var container = this.getEventoContainer();
        container.removeAll();
        container.push({
            title: 'Evento',
            xtype: 'archiveList'
        });
        Ext.getCmp('backButtonId').hide();
        this.toggleNav();
    },
    alertTap: function () {
        var container = this.getEventoContainer();
        container.removeAll();
        container.push({
            title: 'Evento',
            xtype: 'alertList'
        });
        Ext.getCmp('backButtonId').hide();
        this.toggleNav();
    },
    myaccountTap: function () {
        this.getEventoContainer().push({
            title: 'Evento',
            xtype: 'myaccount'
        });
        Ext.getCmp('backButtonId').hide();
        this.toggleNav();

    },
    logoutTap: function () {
        Ext.Msg.confirm('', 'Quit ?', function (id, value) {
            if (id === 'yes')
                Ext.Viewport.removeAll();
            else
                this.toggleNav();
        }, this);
    },
    
    toggleNav: function () {
        var mainEl = this.getEventoContainer().element;
        if (mainEl.hasCls('out')) {
            mainEl.removeCls('out').addCls('in');
        } else {
            mainEl.removeCls('in').addCls('out');
        }
    }

});