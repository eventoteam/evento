Ext.define('Evento.model.Alert', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'date',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'detail',
                type: 'string'
            }
        ]
    }
});