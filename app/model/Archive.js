Ext.define('Evento.model.Archive', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'date',
                type: 'string'
            },
            {
                name: 'timestart',
                type: 'string'
            },
            {
                name: 'timeend',
                type: 'string'
            },
            {
                name: 'venue',
                type: 'string'
            },
            {
                name: 'type',
                type: 'string'
            },
            {
                name: 'agenda',
                type: 'array'
            },
            {
                name: 'attachment',
                type: 'array'
            },
            {
                name: 'note',
                type: 'array'
            }
        ]
    }
});